from abc import ABC, abstractmethod

"""
Определяем объект какого класса созавать в зависимости от контекста
"""


class Document(ABC):
    @abstractmethod
    def show(self):
        pass


class ODFDocument(Document):
    def show(self):
        print("Open document format")


class MSOfficeDocument(Document):
    def show(self):
        print("MS Office document format")


class DocumentFactory:
    _documents = {
        'odf': ODFDocument,
        'doc': MSOfficeDocument,
    }

    # Да, все настолько просто
    def create_document(self, type):
        if type in self._documents:
            return self._documents[type]()
        return None


if __name__ == "__main__":
    factory = DocumentFactory()

    factory.create_document('odf').show()
    factory.create_document('doc').show()
