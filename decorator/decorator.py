"""
Писать паттерн декоратора в питоне и не использовать встроенные декоратор?
ГРЯЗНЫЙ ИЗВРАЩЕНЕЦ
"""


class Man:
    def __init__(self, name):
        self._name = name

    def hello(self):
        print(f"Hello! My name is {self._name}.")


class Jetpacker:
    def __init__(self, man):
        self._man = man

    def __getattr__(self, item):
        return getattr(self._man, item)

    def fly(self):
        print(f"I AM THE {self._man._name}! TO INFINITY AND BEYOND!")


if __name__ == "__main__":
    buzz_lightyear = Jetpacker(Man("Buzz Lightyear"))
    buzz_lightyear.hello()
    buzz_lightyear.fly()
