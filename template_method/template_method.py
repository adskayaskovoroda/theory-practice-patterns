from abc import ABC, abstractmethod
from typing import TextIO, Iterable

"""
Так, это асбтрактный класс
Что это такое можешь почитать в инетах, но если ты ленивая жопа, то коротко:
Абстрактный класс запрещает нам создавать экземпляры этого класса
(в питоне запрет действует до тех пор пока есть хотя бы один абстрактный метод - @abstractmethod)
"""


# Калькулятор среднего значения
class AbstractAverageCalculator(ABC):
    def average(self):
        try:
            num_items = 0
            total_sum = 0
            for num in self:
                total_sum += num
                num_items += 1
            if num_items == 0:
                raise RuntimeError("Can't compute the average of zero items.")

            return total_sum / num_items
        finally:
            self.dispose()

    @abstractmethod
    def __next__(self):
        pass

    def __iter__(self):
        return self

    def dispose(self):
        pass


"""
Дальше от этого класса можно унаследоваться и переопределить методы has_next и next_item
В итоге мы можем менять поведение калькулятор в классах, унаследованных от абстрактного
Например калькулятор, который читает числа из файла
"""


class FileAverageCalculator(AbstractAverageCalculator):
    def __init__(self, file: TextIO):
        self.file = file
        self.last_line = self.file.readline()

    def __next__(self):
        if self.last_line == '':
            raise StopIteration

        result = float(self.last_line)
        self.last_line = self.file.readline()
        return result

    def dispose(self):
        self.file.close()


"""
Или калькулятор, который считает через переданный итерируемый объект (типа списка)
"""


class IteratorAverageCalculator(AbstractAverageCalculator):
    def __init__(self, values: Iterable):
        self.values = values

    def __next__(self):
        pass

    def __iter__(self):
        return iter(self.values)


if __name__ == "__main__":
    try:
        calc = AbstractAverageCalculator()
        calc.average()
    except TypeError:
        print("We can't create instance of abstract class")

    print("\nFile Average Calculator")
    calc = FileAverageCalculator(open("template_method_data.txt"))
    print(calc.average())

    print("\nIterator Average Calculator")
    calc = IteratorAverageCalculator([1, 2, 3, 4, 5])
    print(calc.average())
