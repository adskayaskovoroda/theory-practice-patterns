"""
Знаю, пример максимально тупой
Суть в том, что если у вас есть какая-то система классов или чего-то еще
Каждый элементо которой каким-то образом должен взаимодействовать с другими элементами
То фасад - хороший способо определить поведение взаимодействия
И предоставить пользователю лёгкий интерфейс
"""


class Paper:
    def __init__(self, count: int = 0):
        self.count = count

    def draw(self, text):
        if self.count > 0:
            self.count -= 1
            print(text)


class Printer:
    @staticmethod
    def error(msg: str):
        print(f"Error: {msg}")

    def print(self, paper: Paper, text: str):
        if paper.count > 0:
            paper.draw(text)
        else:
            self.error("The printer is out of paper")


class PrinterFacade:
    def __init__(self, paper_count: int = 1):
        self._printer = Printer()
        self._paper = Paper(paper_count)

    def write(self, text: str):
        self._printer.print(self._paper, text)


if __name__ == "__main__":
    printer = PrinterFacade(1)

    printer.write("KILL ALL HUMANS")
    printer.write("KILL ALL HUMANS")
