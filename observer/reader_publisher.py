from observer import Observer, Observable


class Reader(Observer):
    def update(self, observable, *args):
        print("WOW!")


class Publisher(Observable):
    def __init__(self):
        super().__init__()

    def publish_book(self):
        print("Hey, readers, I just publish a new book!")
        self.notify_observers()


"""
Кстати не обязательно чтобы Observer и Observable были разными классами
"""

if __name__ == "__main__":
    reader1 = Reader()
    reader2 = Reader()
    reader3 = Reader()

    publisher = Publisher()
    publisher.add_observer(reader1)
    publisher.add_observer(reader2)
    publisher.add_observer(reader3)

    publisher.publish_book()
