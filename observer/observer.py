from abc import ABC, abstractmethod

"""
Что такое абстрактные классы либо читай сам
Либо я коротко рассказал в паттерне шаблонного метода
"""


class Observer(ABC):
    @abstractmethod
    def update(self, observable, *args):
        pass


class Observable:
    def __init__(self):
        self.__observers = []

    def add_observer(self, observer: Observer):
        if observer not in self.__observers:
            self.__observers.append(observer)

    def delete_observer(self, observer: Observer):
        if observer in self.__observers:
            self.__observers.remove(observer)

    def notify_observers(self, *args):
        for observer in self.__observers:
            observer.update(self, *args)
