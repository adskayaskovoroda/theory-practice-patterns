class Tigger:
    def __new__(cls, *args, **kwargs):
        if not hasattr(cls, "_instance"):
            cls._instance = super().__new__(cls)
            print(">>> Object created!")
        return cls._instance

    def __str__(self):
        return "I'm the only one!"

    def roar(self):
        return "Grrr!"


if __name__ == "__main__":
    a = Tigger()
    b = Tigger()

    print(f"a ID is {id(a)}, b ID is {id(b)}")
    print(f"a is b? {a is b}")
