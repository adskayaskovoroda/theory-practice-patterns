"""
Доспустим какой-то дурак описал класс утки и индейки без идинакого интерфейса

Переписывать код - не вариант. Долго. Муторно. Лень
По заданию нам нужно сделать так, чтобы метод go_go_duck мог работать с индейками
"""


class Duck:
    def quack(self):
        print("Quack")

    def fly(self):
        print("I'm flying")


class Turkey:
    # о боже, индейка не может в quack
    def gobble(self):
        print("Gobble gobble")

    def fly(self):
        print("I'm flying a short distance")


def go_go_duck(duck: Duck):
    duck.quack()
    duck.fly()


class TurkeyToDuckAdapter:
    def __init__(self, adaptee: Turkey):
        self.adaptee = adaptee

    # а нет, может
    def quack(self):
        self.adaptee.gobble()

    def fly(self):
        for _ in range(5):
            self.adaptee.fly()


if __name__ == "__main__":
    turkey = TurkeyToDuckAdapter(Turkey())
    go_go_duck(turkey)
